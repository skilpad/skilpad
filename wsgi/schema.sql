create table if not exists route (
  id integer primary key autoincrement,
  date date not null,
  data text not null
);
create table if not exists addressbook (
  id integer primary key autoincrement,
  firstname text not null,
  lastname text not null,
  address text not null,
  phone number not null
);

