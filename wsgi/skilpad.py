#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
    skilpad
    ~~~~~~~
"""

from sqlite3 import dbapi2 as sqlite3
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash, send_from_directory
from werkzeug import secure_filename
import os, csv

app = Flask(__name__)

# Load default config and override config from an environment variable
app.config.update(dict(
    DATABASE='tmp/route.db',
    DEBUG=True,
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)

app.config['UPLOAD_FOLDER'] = 'tmp'
ALLOWED_EXTENSIONS = set(['txt', 'csv'])

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

def fill_addr_b_db():
    with app.app_context():
            db = get_db()
            with open('adress_book.csv','rb') as f:
                address = csv.DictReader(f, delimiter=';')
                req = [(i['Nom'].decode('UTF-8'), i['Prénom'].decode('UTF-8'), i['Adresse'].decode('UTF-8'), i['Numéros Téléphone']) for i in address]


            # Clean empty lines (Lastname is REQUIRED)
            req = [x for x in req if x[0]  != '']

            # Don't dupplicate
            reqn = []
            for idx, i in enumerate(req):
                cur = db.execute('SELECT * FROM addressbook WHERE lastname="' + i[1] + '" AND firstname="' + i[0] + '"')

                if (len(cur.fetchall()) < 1):
                    reqn.append(i)

            db.executemany("INSERT INTO addressbook (firstname, lastname, address, phone) VALUES (?, ?, ?, ?);", reqn)
            db.commit()

@app.route('/ab', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            fill_addr_b_db()
            return redirect('/')
    return render_template('upload.html')

def show_addressbook():
    print 'YYYYYYYYY'
    db = get_db()
    cur = db.execute('SELECT * FROM addressbook ORDER BY id DESC')
    addresses = cur.fetchall()
    print addresses
    return render_template('show_address_book.html', addresses=addresses)

def connect_db():
    """Connects to the specific database."""
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv


def init_db():
    """Creates the database tables."""
    with app.app_context():
        db = get_db()
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()


def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db


@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()


@app.route('/')
@app.route("/index")
def show_entries():
    db = get_db()
    cur = db.execute('select date, data from route order by id desc')
    entries = cur.fetchall()
    cur = db.execute('SELECT * FROM addressbook ORDER BY id DESC')
    addresses = cur.fetchall()
    return render_template('show_entries.html', entries=entries, addresses=addresses)


@app.route('/add', methods=['POST'])
def add_entry():
    data = ''
    for i in request.form.getlist('data'):
        data += i + ';'
    if not session.get('logged_in'):
        abort(401)
    db = get_db()
    db.execute('insert into route (date, data) values (?, ?)',
                 [request.form['date'], data])
    db.commit()
    flash('New entry was successfully posted')
    return redirect(url_for('show_entries'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('show_entries'))
    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('show_entries'))


if __name__ == '__main__':
    init_db()
    #fill_addr_b_db()
    app.run()
