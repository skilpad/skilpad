from setuptools import setup

setup(name='skilpad',
      version='0.2',
      description='Openshift skilpad',
      author='shaiton',
      url='gitorious.org/skilpad/skilpad',
      install_requires=['Flask>=0.7.2', 'MarkupSafe'],
     )
